﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KalkulatorNaukowy
{
    public partial class Kalkulator : Form
    {
        public Kalkulator()
        {
            InitializeComponent();
        }

        bool afterError = false;
        bool naukowy = false;
        char znak;
        char[] znakiPodzialu = { '-', '+', '*', '/'};




        private void errorClear()
        {
            ekran.Font = new System.Drawing.Font("Franklin Gothic Medium", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            if (afterError == true)
            {
                ekran.Text = "";
                afterError = false;
            }
        }

        private void b0_Click(object sender, EventArgs e)
        {
            errorClear();
            ekran.Text += "0";
        }

        private void b1_Click(object sender, EventArgs e)
        {
            errorClear();
            ekran.Text += "1";
        }

        private void b2_Click(object sender, EventArgs e)
        {
            errorClear();
            ekran.Text += "2";
        }

        private void b3_Click(object sender, EventArgs e)
        {
            errorClear();
            ekran.Text += "3";
        }

        private void b4_Click(object sender, EventArgs e)
        {
            errorClear();
            ekran.Text += "4";
        }

        private void b5_Click(object sender, EventArgs e)
        {
            errorClear();
            ekran.Text += "5";
        }

        private void b6_Click(object sender, EventArgs e)
        {
            errorClear();
            ekran.Text += "6";
        }

        private void b7_Click(object sender, EventArgs e)
        {
            errorClear();
            ekran.Text += "7";
        }

        private void b8_Click(object sender, EventArgs e)
        {
            errorClear();
            ekran.Text += "8";
        }

        private void b9_Click(object sender, EventArgs e)
        {
            errorClear();
            ekran.Text += "9";
        }

        private void bComma_Click(object sender, EventArgs e)
        {
            errorClear();
            ekran.Text += ",";
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            errorClear();
            if (ekran.Text != "")
            {
                ekran.Text += "+";
                znak = '+';
            }
        }

        private void bSub_Click(object sender, EventArgs e)
        {
            errorClear();
            ekran.Text += "-";
            znak = '-';
        }

        private void bMul_Click(object sender, EventArgs e)
        {
            errorClear();
            if (ekran.Text != "")
            {
                ekran.Text += "*";
                znak = '*';
            }
        }

        private void bDiv_Click(object sender, EventArgs e)
        {
            errorClear();
            if (ekran.Text != "")
            {
                ekran.Text += "/";
                znak = '/';
            }
        }

        private void bClear_Click(object sender, EventArgs e)
        {
            try
            {
                ekran.Text = ekran.Text.Substring(0, ekran.Text.Length - 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                ekran.Text = "Za daleko :P";
                afterError = true;
            }
            
        }

        private void bClearAll_Click(object sender, EventArgs e)
        {
            ekran.Text = "";
        }

        private void bSwitch_Click(object sender, EventArgs e)
        {
            if (naukowy == false) 
            {
                bAdd.Visible = false;
                bSub.Visible = false;
                bMul.Visible = false;
                bDiv.Visible = false;
                bDelta.Visible = true;
                bPotega.Visible = true;
                bPotega2.Visible = true;
                bPierwiastek.Visible = true;
                naukowy = true;
            }
            else
            {
                bAdd.Visible = true;
                bSub.Visible = true;
                bMul.Visible = true;
                bDiv.Visible = true;
                bDelta.Visible = false;
                bPotega.Visible = false;
                bPotega2.Visible = false;
                bPierwiastek.Visible = false;
                naukowy = false;
            }

        }

        private void bDelta_Click(object sender, EventArgs e)
        {
            string dzialanie = ekran.Text;
            string[] liczby = dzialanie.Split(znakiPodzialu);
            float a, b, c, wynik;
            try
            {
                float.TryParse(liczby[0], out a);
                float.TryParse(liczby[1], out b);
                float.TryParse(liczby[2], out c);

                wynik = (b * b) - (4 * a * c);
                ekran.Text = wynik.ToString();
            }
            catch (System.IndexOutOfRangeException)
            {
                ekran.Font = new System.Drawing.Font("Franklin Gothic Medium", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                ekran.Text = "Delta musi mieć 3 liczby";
                afterError = true;
            }
        }

        private void bEqual_Click(object sender, EventArgs e)
        {
            string dzialanie = ekran.Text;
            string[] liczby = dzialanie.Split(znakiPodzialu);
            float a, b, wynik;
            try
            {
                float.TryParse(liczby[0], out a);
                float.TryParse(liczby[1], out b);

                if (znak == '+')
                {
                    wynik = a + b;
                    ekran.Text = wynik.ToString();
                }
                if (znak == '-')
                {
                    wynik = a - b;
                    ekran.Text = wynik.ToString();
                }
                if (znak == '*')
                {
                    wynik = a * b;
                    ekran.Text = wynik.ToString();
                }
                if (znak == '/')
                {
                    if (b != 0)
                    {
                        wynik = a / b;
                        ekran.Text = wynik.ToString();
                    }
                    else
                    {
                        ekran.Font = new System.Drawing.Font("Franklin Gothic Medium", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        ekran.Text = "Nie dziel przez 0";
                        afterError = true;
                    }

                }


            }
            catch (Exception)
            {
                ekran.Font = new System.Drawing.Font("Franklin Gothic Medium", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                ekran.Text = "Wystąpił nieznany błąd";
                afterError = true;
            }
        }

        private void bPotega_Click(object sender, EventArgs e)
        {
            string dzialanie = ekran.Text;
            string[] liczby = dzialanie.Split(znakiPodzialu);
            float a, wynik;
            try
            {
                if (liczby[1] != null)
                {
                    ekran.Font = new System.Drawing.Font("Franklin Gothic Medium", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    ekran.Text = "Wprowadź jedna liczbe";
                    afterError = true;
                    return;
                }
                float.TryParse(liczby[0], out a);

                wynik = a * a;
                ekran.Text = wynik.ToString();
            }
            catch (Exception)
            {
                ekran.Font = new System.Drawing.Font("Franklin Gothic Medium", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                ekran.Text = "Wprowadź jedna liczbe";
                afterError = true;
            }
        }

        private void bPotega2_Click(object sender, EventArgs e)
        {
            string dzialanie = ekran.Text;
            string[] liczby = dzialanie.Split(znakiPodzialu);
            float a, wynik = 10;
            try
            {
                if (liczby[1] != null)
                {
                    ekran.Font = new System.Drawing.Font("Franklin Gothic Medium", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    ekran.Text = "Wprowadź jedna liczbe";
                    afterError = true;
                    return;
                }
                float.TryParse(liczby[0], out a);
                
                for (int i = 0; i < a-1; i++)
                {
                    wynik *= 10;
                }
                ekran.Text = wynik.ToString();
            }
            catch (Exception)
            {
                ekran.Font = new System.Drawing.Font("Franklin Gothic Medium", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                ekran.Text = "Wprowadź jedna liczbe";
                afterError = true;
            }
        }

        private void bPierwiastek_Click(object sender, EventArgs e)
        {
            string dzialanie = ekran.Text;
            string[] liczby = dzialanie.Split(znakiPodzialu);
            double a, wynik;
            try
            {
                if (liczby[1] != null) 
                {
                    ekran.Font = new System.Drawing.Font("Franklin Gothic Medium", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    ekran.Text = "Wprowadź jedna liczbe";
                    afterError = true;
                    return;
                }
                double.TryParse(liczby[0], out a);

                wynik = Math.Sqrt((double)a);
                ekran.Text = wynik.ToString();
            }
            catch (Exception)
            {
                ekran.Font = new System.Drawing.Font("Franklin Gothic Medium", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                ekran.Text = "Wprowadź jedna liczbe";
                afterError = true;
            }
        }
    }
}
