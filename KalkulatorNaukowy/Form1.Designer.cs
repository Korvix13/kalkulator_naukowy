﻿namespace KalkulatorNaukowy
{
    partial class Kalkulator
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.b7 = new System.Windows.Forms.Button();
            this.ekran = new System.Windows.Forms.TextBox();
            this.b8 = new System.Windows.Forms.Button();
            this.b9 = new System.Windows.Forms.Button();
            this.b4 = new System.Windows.Forms.Button();
            this.b5 = new System.Windows.Forms.Button();
            this.b6 = new System.Windows.Forms.Button();
            this.b1 = new System.Windows.Forms.Button();
            this.b2 = new System.Windows.Forms.Button();
            this.b3 = new System.Windows.Forms.Button();
            this.bComma = new System.Windows.Forms.Button();
            this.b0 = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bAdd = new System.Windows.Forms.Button();
            this.bSub = new System.Windows.Forms.Button();
            this.bMul = new System.Windows.Forms.Button();
            this.bDiv = new System.Windows.Forms.Button();
            this.bEqual = new System.Windows.Forms.Button();
            this.bSwitch = new System.Windows.Forms.Button();
            this.bClearAll = new System.Windows.Forms.Button();
            this.bDelta = new System.Windows.Forms.Button();
            this.bPotega = new System.Windows.Forms.Button();
            this.bPotega2 = new System.Windows.Forms.Button();
            this.bPierwiastek = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // b7
            // 
            this.b7.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b7.Location = new System.Drawing.Point(12, 98);
            this.b7.Name = "b7";
            this.b7.Size = new System.Drawing.Size(80, 80);
            this.b7.TabIndex = 0;
            this.b7.Text = "7";
            this.b7.UseVisualStyleBackColor = true;
            this.b7.Click += new System.EventHandler(this.b7_Click);
            // 
            // ekran
            // 
            this.ekran.Font = new System.Drawing.Font("Franklin Gothic Medium", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ekran.Location = new System.Drawing.Point(12, 12);
            this.ekran.Name = "ekran";
            this.ekran.Size = new System.Drawing.Size(424, 80);
            this.ekran.TabIndex = 1;
            this.ekran.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // b8
            // 
            this.b8.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b8.Location = new System.Drawing.Point(98, 98);
            this.b8.Name = "b8";
            this.b8.Size = new System.Drawing.Size(80, 80);
            this.b8.TabIndex = 2;
            this.b8.Text = "8";
            this.b8.UseVisualStyleBackColor = true;
            this.b8.Click += new System.EventHandler(this.b8_Click);
            // 
            // b9
            // 
            this.b9.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b9.Location = new System.Drawing.Point(184, 98);
            this.b9.Name = "b9";
            this.b9.Size = new System.Drawing.Size(80, 80);
            this.b9.TabIndex = 3;
            this.b9.Text = "9";
            this.b9.UseVisualStyleBackColor = true;
            this.b9.Click += new System.EventHandler(this.b9_Click);
            // 
            // b4
            // 
            this.b4.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b4.Location = new System.Drawing.Point(12, 184);
            this.b4.Name = "b4";
            this.b4.Size = new System.Drawing.Size(80, 80);
            this.b4.TabIndex = 4;
            this.b4.Text = "4";
            this.b4.UseVisualStyleBackColor = true;
            this.b4.Click += new System.EventHandler(this.b4_Click);
            // 
            // b5
            // 
            this.b5.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b5.Location = new System.Drawing.Point(98, 184);
            this.b5.Name = "b5";
            this.b5.Size = new System.Drawing.Size(80, 80);
            this.b5.TabIndex = 5;
            this.b5.Text = "5";
            this.b5.UseVisualStyleBackColor = true;
            this.b5.Click += new System.EventHandler(this.b5_Click);
            // 
            // b6
            // 
            this.b6.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b6.Location = new System.Drawing.Point(184, 184);
            this.b6.Name = "b6";
            this.b6.Size = new System.Drawing.Size(80, 80);
            this.b6.TabIndex = 6;
            this.b6.Text = "6";
            this.b6.UseVisualStyleBackColor = true;
            this.b6.Click += new System.EventHandler(this.b6_Click);
            // 
            // b1
            // 
            this.b1.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b1.Location = new System.Drawing.Point(12, 270);
            this.b1.Name = "b1";
            this.b1.Size = new System.Drawing.Size(80, 80);
            this.b1.TabIndex = 7;
            this.b1.Text = "1";
            this.b1.UseVisualStyleBackColor = true;
            this.b1.Click += new System.EventHandler(this.b1_Click);
            // 
            // b2
            // 
            this.b2.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b2.Location = new System.Drawing.Point(98, 270);
            this.b2.Name = "b2";
            this.b2.Size = new System.Drawing.Size(80, 80);
            this.b2.TabIndex = 8;
            this.b2.Text = "2";
            this.b2.UseVisualStyleBackColor = true;
            this.b2.Click += new System.EventHandler(this.b2_Click);
            // 
            // b3
            // 
            this.b3.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b3.Location = new System.Drawing.Point(184, 270);
            this.b3.Name = "b3";
            this.b3.Size = new System.Drawing.Size(80, 80);
            this.b3.TabIndex = 9;
            this.b3.Text = "3";
            this.b3.UseVisualStyleBackColor = true;
            this.b3.Click += new System.EventHandler(this.b3_Click);
            // 
            // bComma
            // 
            this.bComma.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bComma.Location = new System.Drawing.Point(12, 356);
            this.bComma.Name = "bComma";
            this.bComma.Size = new System.Drawing.Size(80, 80);
            this.bComma.TabIndex = 10;
            this.bComma.Text = ",";
            this.bComma.UseVisualStyleBackColor = true;
            this.bComma.Click += new System.EventHandler(this.bComma_Click);
            // 
            // b0
            // 
            this.b0.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b0.Location = new System.Drawing.Point(98, 356);
            this.b0.Name = "b0";
            this.b0.Size = new System.Drawing.Size(80, 80);
            this.b0.TabIndex = 11;
            this.b0.Text = "0";
            this.b0.UseVisualStyleBackColor = true;
            this.b0.Click += new System.EventHandler(this.b0_Click);
            // 
            // bClear
            // 
            this.bClear.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bClear.Location = new System.Drawing.Point(184, 356);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(80, 80);
            this.bClear.TabIndex = 12;
            this.bClear.Text = "C";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bAdd
            // 
            this.bAdd.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bAdd.Location = new System.Drawing.Point(270, 98);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(80, 80);
            this.bAdd.TabIndex = 13;
            this.bAdd.Text = "+";
            this.bAdd.UseVisualStyleBackColor = true;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // bSub
            // 
            this.bSub.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bSub.Location = new System.Drawing.Point(356, 98);
            this.bSub.Name = "bSub";
            this.bSub.Size = new System.Drawing.Size(80, 80);
            this.bSub.TabIndex = 14;
            this.bSub.Text = "-";
            this.bSub.UseVisualStyleBackColor = true;
            this.bSub.Click += new System.EventHandler(this.bSub_Click);
            // 
            // bMul
            // 
            this.bMul.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bMul.Location = new System.Drawing.Point(270, 184);
            this.bMul.Name = "bMul";
            this.bMul.Size = new System.Drawing.Size(80, 80);
            this.bMul.TabIndex = 15;
            this.bMul.Text = "X";
            this.bMul.UseVisualStyleBackColor = true;
            this.bMul.Click += new System.EventHandler(this.bMul_Click);
            // 
            // bDiv
            // 
            this.bDiv.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDiv.Location = new System.Drawing.Point(356, 184);
            this.bDiv.Name = "bDiv";
            this.bDiv.Size = new System.Drawing.Size(80, 80);
            this.bDiv.TabIndex = 16;
            this.bDiv.Text = "/";
            this.bDiv.UseVisualStyleBackColor = true;
            this.bDiv.Click += new System.EventHandler(this.bDiv_Click);
            // 
            // bEqual
            // 
            this.bEqual.Font = new System.Drawing.Font("Franklin Gothic Medium", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bEqual.Location = new System.Drawing.Point(270, 270);
            this.bEqual.Name = "bEqual";
            this.bEqual.Size = new System.Drawing.Size(166, 80);
            this.bEqual.TabIndex = 17;
            this.bEqual.Text = "=";
            this.bEqual.UseVisualStyleBackColor = true;
            this.bEqual.Click += new System.EventHandler(this.bEqual_Click);
            // 
            // bSwitch
            // 
            this.bSwitch.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bSwitch.Location = new System.Drawing.Point(356, 356);
            this.bSwitch.Name = "bSwitch";
            this.bSwitch.Size = new System.Drawing.Size(80, 80);
            this.bSwitch.TabIndex = 18;
            this.bSwitch.Text = "Switch";
            this.bSwitch.UseVisualStyleBackColor = true;
            this.bSwitch.Click += new System.EventHandler(this.bSwitch_Click);
            // 
            // bClearAll
            // 
            this.bClearAll.Font = new System.Drawing.Font("Franklin Gothic Medium", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bClearAll.Location = new System.Drawing.Point(270, 356);
            this.bClearAll.Name = "bClearAll";
            this.bClearAll.Size = new System.Drawing.Size(80, 80);
            this.bClearAll.TabIndex = 19;
            this.bClearAll.Text = "CC";
            this.bClearAll.UseVisualStyleBackColor = true;
            this.bClearAll.Click += new System.EventHandler(this.bClearAll_Click);
            // 
            // bDelta
            // 
            this.bDelta.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bDelta.Location = new System.Drawing.Point(270, 98);
            this.bDelta.Name = "bDelta";
            this.bDelta.Size = new System.Drawing.Size(80, 80);
            this.bDelta.TabIndex = 20;
            this.bDelta.Text = "DELTA";
            this.bDelta.UseVisualStyleBackColor = true;
            this.bDelta.Visible = false;
            this.bDelta.Click += new System.EventHandler(this.bDelta_Click);
            // 
            // bPotega
            // 
            this.bPotega.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bPotega.Location = new System.Drawing.Point(356, 98);
            this.bPotega.Name = "bPotega";
            this.bPotega.Size = new System.Drawing.Size(80, 80);
            this.bPotega.TabIndex = 21;
            this.bPotega.Text = "x^2";
            this.bPotega.UseVisualStyleBackColor = true;
            this.bPotega.Visible = false;
            this.bPotega.Click += new System.EventHandler(this.bPotega_Click);
            // 
            // bPotega2
            // 
            this.bPotega2.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bPotega2.Location = new System.Drawing.Point(270, 184);
            this.bPotega2.Name = "bPotega2";
            this.bPotega2.Size = new System.Drawing.Size(80, 80);
            this.bPotega2.TabIndex = 22;
            this.bPotega2.Text = "10^x";
            this.bPotega2.UseVisualStyleBackColor = true;
            this.bPotega2.Visible = false;
            this.bPotega2.Click += new System.EventHandler(this.bPotega2_Click);
            // 
            // bPierwiastek
            // 
            this.bPierwiastek.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bPierwiastek.Location = new System.Drawing.Point(356, 184);
            this.bPierwiastek.Name = "bPierwiastek";
            this.bPierwiastek.Size = new System.Drawing.Size(80, 80);
            this.bPierwiastek.TabIndex = 23;
            this.bPierwiastek.Text = "√";
            this.bPierwiastek.UseVisualStyleBackColor = true;
            this.bPierwiastek.Visible = false;
            this.bPierwiastek.Click += new System.EventHandler(this.bPierwiastek_Click);
            // 
            // Kalkulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(447, 450);
            this.Controls.Add(this.bPierwiastek);
            this.Controls.Add(this.bPotega2);
            this.Controls.Add(this.bPotega);
            this.Controls.Add(this.bDelta);
            this.Controls.Add(this.bClearAll);
            this.Controls.Add(this.bSwitch);
            this.Controls.Add(this.bEqual);
            this.Controls.Add(this.bDiv);
            this.Controls.Add(this.bMul);
            this.Controls.Add(this.bSub);
            this.Controls.Add(this.bAdd);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.b0);
            this.Controls.Add(this.bComma);
            this.Controls.Add(this.b3);
            this.Controls.Add(this.b2);
            this.Controls.Add(this.b1);
            this.Controls.Add(this.b6);
            this.Controls.Add(this.b5);
            this.Controls.Add(this.b4);
            this.Controls.Add(this.b9);
            this.Controls.Add(this.b8);
            this.Controls.Add(this.ekran);
            this.Controls.Add(this.b7);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Kalkulator";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button b7;
        private System.Windows.Forms.TextBox ekran;
        private System.Windows.Forms.Button b8;
        private System.Windows.Forms.Button b9;
        private System.Windows.Forms.Button b4;
        private System.Windows.Forms.Button b5;
        private System.Windows.Forms.Button b6;
        private System.Windows.Forms.Button b1;
        private System.Windows.Forms.Button b2;
        private System.Windows.Forms.Button b3;
        private System.Windows.Forms.Button bComma;
        private System.Windows.Forms.Button b0;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Button bAdd;
        private System.Windows.Forms.Button bSub;
        private System.Windows.Forms.Button bMul;
        private System.Windows.Forms.Button bDiv;
        private System.Windows.Forms.Button bEqual;
        private System.Windows.Forms.Button bSwitch;
        private System.Windows.Forms.Button bClearAll;
        private System.Windows.Forms.Button bDelta;
        private System.Windows.Forms.Button bPotega;
        private System.Windows.Forms.Button bPotega2;
        private System.Windows.Forms.Button bPierwiastek;
    }
}

